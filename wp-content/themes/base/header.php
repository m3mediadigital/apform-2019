<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>
<?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
?>
</title>
<?php wp_head(); ?>
<link href='<?php bloginfo( 'template_url' ); ?>/images/favicon.ico' type="image/x-icon" rel="icon" />
<meta name="keywords" content="apform, fabrica, industria, comercio, mobiliario, moveis, escolar, escritorio, cadeiras, mesas, armarios, estantes, quadro, painel, regional, natal, parnamirim, rn">
<meta name="description" content="A APFORM atua no mercado regional fabricando mobiliário escolar e para escritório.">
<meta name="author" content="Nova M3">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="https://code.jquery.com/jquery.js"></script> 
<link href="<?php bloginfo( 'template_url' ); ?>/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="<?php bloginfo('stylesheet_url'); ?>?<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/nivo-slider.css" type="text/css" />
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/prettyPhoto.css" type="text/css" />
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/component.css" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css" type="text/css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-32450609-44', 'auto');
  ga('send', 'pageview');

</script>


<?php
 $url = $_SERVER['REQUEST_URI'];

 if ($url == "/assentos/" or  "/mesas-e-armarios/" or  "/escolar/") {
 	$scroll = "";
 } else {
 	$scroll = "scroll";
 };

?>


</head>
<body <?php body_class(); ?>>
	<?php /*
<div id="banner-landingpage"<?php if( is_front_page() ) : ?> class="none"<?php endif; ?>>
    <a href="https://apform.com.br/landing/" title="Conheça nossas linhas">
        <img src="<?php bloginfo( 'template_url' ); ?>/images/banner-landingpage2.jpg">
    </a>
</div>
*/ ?>	
            
            
	<div class="header">


		<div class="topLine"></div>
		<div class="topo">
			<div class="container">
				<div class="row menu-flex">
					<div class="menu-mobile hidden-md hidden-lg">
			            <div id="dl-menu" class="dl-menuwrapper">
			                <button class="dl-trigger">Open Menu</button>
			                <ul class="dl-menu">
	                      		<li><a href="<?php echo get_option('home'); ?>/" >HOME</a></li>
								<li><a href="<?php echo get_option('home'); ?>/#produtos" class="<?php echo $scroll; ?>" >PRODUTOS</a></li>
								<li><a href="<?php echo get_option('home'); ?>/#atuacao" class="<?php echo $scroll; ?>" >ATUAÇÃO</a></li>
								<li><a href="<?php echo get_option('home'); ?>/#aempresa" class="<?php echo $scroll; ?>" >EMPRESA</a></li>
								<li><a href="<?php echo get_option('home'); ?>/#contato" class="<?php echo $scroll; ?>" >CONTATO</a></li>
			                </ul>
			            </div>
		            </div>
					<a href="<?php echo get_option('home'); ?>" class="topo-logo">
						<!-- <img class="logo" src="<?php //bloginfo( 'template_url' ); ?>/images/logo.png"> -->
						<img class="logo" src="<?php bloginfo( 'template_url' ); ?>/images/logo_new.jpeg">
					</a>

					<nav class="nav hidden-xs hidden-sm">
						<ul>
							<li><a href="<?php echo get_option('home'); ?>/" >HOME</a></li>
							<li><a href="<?php echo get_option('home'); ?>/#produtos" class="<?php echo $scroll; ?>" >PRODUTOS</a></li>
							<li><a href="<?php echo get_option('home'); ?>/#atuacao" class="<?php echo $scroll; ?>" >ATUAÇÃO</a></li>
							<li><a href="<?php echo get_option('home'); ?>/#aempresa" class="<?php echo $scroll; ?>" >EMPRESA</a></li>
							<li><a href="<?php echo get_option('home'); ?>/#contato" class="<?php echo $scroll; ?>" >CONTATO</a></li>
						</ul>
					</nav>

					<div class="boxFone hidden-xs hidden-sm">
						<img src="<?php bloginfo( 'template_url' ); ?>/images/icon-tel.png"><p><a href="tel:084991032534" class="link-header"> (84) 99103-2534

 </a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="selo-whatsapp">
    <a href="https://api.whatsapp.com/send?phone=5584991032534&text=Oi!">
        <img src="https://apform.com.br/wp-content/uploads/2019/04/Selo-Whats.png">
    </a>
</div>
	<!-- <div class="marginTop"></div> -->