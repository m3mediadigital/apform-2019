$(document).ready(function() {
	
    if (window.location.hash!=''){
        $('html, body').animate({
            scrollTop: $(window.location.hash).offset().top -50
        }, 2000);
    }


	$('#dl-menu').dlmenu();
	
	
    $('#slider').nivoSlider({
       	effect: 'random',               // Specify sets like: 'fold,fade,sliceDown'
        slices: 15,                     // For slice animations
        boxCols: 8,                     // For box animations
        boxRows: 4,                     // For box animations
        animSpeed: 500,                 // Slide transition speed
        pauseTime: 3000,                // How long each slide will show
        startSlide: 0,                  // Set starting Slide (0 index)
        directionNav: true,             // Next & Prev navigation
        controlNav: false,               // 1,2,3... navigation
        controlNavThumbs: false,        // Use thumbnails for Control Nav
        pauseOnHover: true,             // Stop animation while hovering
        manualAdvance: false,           // Force manual transitions
        prevText: '<div class="arrowLeft"></div>',               // Prev directionNav text
        nextText: '<div class="arrowRight"></div>',               // Next directionNav text
        randomStart: false,             // Start on a random slide
        beforeChange: function(){},     // Triggers before a slide transition
        afterChange: function(){},      // Triggers after a slide transition
        slideshowEnd: function(){},     // Triggers after all slides have been shown
        lastSlide: function(){},        // Triggers when last slide is shown
        afterLoad: function(){}         // Triggers when slider has loaded
    });

    new WOW().init();

    

    // }

    // var markerNatal = '';
    // if(markers.length > 0) {
    //     for(var i = 0; i < markers.length; i++) {
    //         if(markers[i].slug == 'natal') {
    //             markerNatal = markers[i]
    //         } else {
    //             markerMap(markers[i]);
    //         }
    //     }
    // }
    // markerMap(markerNatal);
    // var markers = [
    //     <?php foreach($cities as $slug => $name): ?>
    //         {
    //             slug: '<?=$slug?>',
    //             lat: '<?=$settings['latitude_' . $slug]?>', 
    //             lng: '<?=$settings['longitude_' . $slug]?>', 
    //             address:  '<?=$settings['endereco_' . $slug]?>',
    //             phone:  '<?=$settings['phone_' . $slug]?>',
    //             google_maps_url:  '<?=$settings['google_place_' . $slug]?>', 
    //         },
    //     <?php endforeach; ?>
    // ];

    // Rezize do menu <--

    var scroll = 100;

    $(function(){   
        var nav = $('.header');   
        $(window).scroll(function () { 
            if ($(this).scrollTop() > scroll) { 
                nav.addClass("headerRoll"); 
            } else { 
                nav.removeClass("headerRoll"); 
            } 
        });  
    });

    $(function(){   
        var nav = $('.header li');   
        $(window).scroll(function () { 
            if ($(this).scrollTop() > scroll) { 
                nav.addClass("liRoll"); 
            } else { 
                nav.removeClass("liRoll"); 
            } 
        });  
    });

    $(function(){   
        var nav = $('.header li a');   
        $(window).scroll(function () { 
            if ($(this).scrollTop() > scroll) { 
                nav.addClass("aRoll"); 
            } else { 
                nav.removeClass("aRoll"); 
            } 
        });  
    });

    $(function(){   
        var nav = $('.header .logo');   
        $(window).scroll(function () { 
            if ($(this).scrollTop() > scroll) { 
                nav.addClass("logoRoll"); 
            } else { 
                nav.removeClass("logoRoll"); 
            } 
        });  
    });

    $(function(){   
        var nav = $('.header .boxFone');   
        $(window).scroll(function () { 
            if ($(this).scrollTop() > scroll) { 
                nav.addClass("boxFoneRoll"); 
            } else { 
                nav.removeClass("boxFoneRoll"); 
            } 
        });  
    });


    $('#carousel').carousel()

    $("a[rel^='prettyPhoto']").prettyPhoto();

    

    $(".scroll").click(function(event){        
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
   });
    
    
    
});


// configurações do modal e do banner flutuante// jessuir aprovou essa função
(function ($) {
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length,c.length);
            }
        }
        return false;
    }

    var modalLandingPage = getCookie('modal.landingpage');

    if(modalLandingPage) {
        $('#banner-landingpage').removeClass('none');
    } else {
        $('#modalLadingpage').modal('show');
        $('#banner-landingpage').addClass('none');
        
        $(document).ready(function() {
            $('#modalLadingpage').on('hide.bs.modal', function (e) {
            $('#banner-landingpage').removeClass('none');
                setCookie('modal.landingpage', true, 1)
            });
        });
    }

    // console.log(typeof modalLandingPage);
})(jQuery)





// setCookie('modal.landingpage', true, 7);




