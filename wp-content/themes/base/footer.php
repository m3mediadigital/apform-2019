<div id="contato" class="rodape">
    <div class="container">
        <div class="row">
            <h1 class="center">ENTRE EM <span class="bold">CONTATO</span></h1>
            <p style="margin-top:25px; margin-bottom:35px;" class="center">Para entrar em contato conosco basta preencher o formulário abaixo ou nos ligar.</p>
            <div class="col-md-6">
                <div class="separadorNoMargin" style="margin-bottom:25px;"></div>
                <h3>FORMULÁRIO DE CONTATO:</h3>
                <?php echo do_shortcode('[contact-form-7 id="6" title="Formulário de contato"]') ?>
            </div>
            <div class="col-md-5 col-md-offset-1">
                <div class="separadorNoMargin" style="margin-bottom:25px;"></div>
                <div class="contatoBox">
                    <img src="<?php bloginfo( 'template_url' ); ?>/images/icon-rodape3.png">
                    <h3>SAC</h3>
                    <p><a href="mailto:sac@apform.com.br" class="link-footer">sac@apform.com.br</a> / <a href="tel:0849880238250" class="link-footer">(84) 98802-3825</a></p>
                </div>
                
                <div class="contatoBox">
                    <img src="<?php bloginfo( 'template_url' ); ?>/images/icon-rodape1.png">
                    <h3>CONTATO</h3>
                    <p><a href="mailto:contato@apform.com.br" class="link-footer">contato@apform.com.br</a> / <a href="tel:084991032534" class="link-footer">(84) 99103-2534</a></p>
                </div>

                <div class="contatoBox">
                    <img src="<?php bloginfo( 'template_url' ); ?>/images/icon-rodape2.png">
                    <h3>ENDEREÇO</h3>
                    <p>R projetada, SN Lote 4 Distrito Industrial 1  Macaíba RN</p>
                </div>
                <div id="map"></div>
                <!-- <a href="https://www.google.com/maps/d/u/0/embed?mid=ztf0RYxjRrww.kRta3E92-xD4?iframe=true&width=640&height=480" rel="prettyPhoto">
                    <img class="mapImage" src="<?php bloginfo( 'template_url' ); ?>/images/map-image2.png">
                </a> -->
            </div>
        </div>
    </div>  
</div>
<div class="rodapeLine"><a href="https://www.novam3.com.br/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/ass.png"></a></div>

<?php if( is_front_page() && 2 == 1) : ?>
    <div class="modal fade" id="modalLadingpage" tabindex="-1" role="dialog" style="z-index: 9999999999;">
        <div id="modal-flutuante" class="modal-dialog" role="document">
            <div class="modal-content">
     
                <div class="modal-body">
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <a href="https://apform.com.br/landing/" title="Conheça nossas linhas">
                        <img src="<?php bloginfo( 'template_url' ); ?>/images/banner-landingpage2.jpg">
                    </a>
                </div>
    
            </div>
        </div>
    </div>
<?php endif; ?>


<script src="https://code.jquery.com/jquery.js"></script> 
<script src="<?php bloginfo( 'template_url' ); ?>/js/bootstrap.min.js"></script> 
<script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.nivo.slider.pack.js" type="text/javascript"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/js/prettyphoto.js" type="text/javascript"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/js/modernizr.custom.js" type="text/javascript"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.dlmenu.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/js/script.js"></script>

<?php wp_footer(); ?>

<script type="text/javascript">
     var map = L.map('map').setView([-5.885035, -35.292086 - 0.009000], 14);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(map);
    var LeafIcon = L.Icon.extend({});
    var customIcon = "<?php bloginfo( 'template_url' ) ?>"+"/images/map-marker.png";
    var _marker = '';

   
    // function markerMap(markerInfo) {
        var content =
            '<div class="d-flex">' +
                '<div class="map-info-content">' +
                    '<a href="https://www.google.com.br/maps/place/Ap+Form+Ind.+E+Com./@-5.8998691,-35.2859282,15z/data=!4m8!1m2!2m1!1sapform!3m4!1s0x7b256b78ae5556b:0xd8c600a955161069!8m2!3d-5.8863737!4d-35.2918722" target="_blank">Como Chegar</a>' +
                '</div>' +
            '</div>';

        var loja = new LeafIcon({
            iconUrl:  customIcon,
            iconSize: [30, 47], // size of the icon
            iconAnchor:   [15, 47], // point of the icon which will correspond to marker's location
            popupAnchor:  [0, -45] // point from which the popup should open relative to the iconAnchor
        });
        // if(_marker != '') {
        //     map.removeLayer(_marker);
        // }
        _marker = L.marker([-5.885035, -35.292086], {icon: loja}).bindPopup(content);
        _marker.addTo(map);
        map.setView(new L.LatLng(-5.885035, -35.292086), 14);
</script>

</body>
</html>