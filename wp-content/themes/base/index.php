<?php get_header(); ?>

<style type="text/css">

.popup-main{
position: relative; }
.popup-overlay{
background: rgba(0,0,0,0.75);
width: 100%; height: 100%;
position: fixed; z-index: 999999; top: 0;
left: 0;
}
.popup-out{ margin-left: auto; z-index: 999999; margin-right: auto; position: fixed; width: 100%;
	top: 0;
}
.popup-inner{ position: absolute; width: 75%;
	left: 50%; margin-left: -37.5%; height: 100vh;
}
.popup-body{
	position: relative;
	top: 50%;
	transform: translateY(-50%);
}
.popup-body.video{
	max-width: 100%; 
}
.popup-body.image{
	//max-width: 500px; 
	max-width:810px;
}
.popup-body img{
	max-width: 100%; 
}
.popup-close{ position: absolute; right: -18px;
	top: -17px;
}	
</style>
<div id="topo"></div>

<?php
$active = 1;
//$testdate = (time() < strtotime("2017-09-01 03:00:00")); $embed = 'qm_9fLbsbak';
$link = 'https://www.fnde.gov.br/phocadownload/compras_governamentais/compras_nacionais/pregoes_eletronicos/2017/10-2017/SEI_FNDE%20-%200850931%20-%20Ata%20de%20Registro%20de%20Preos%2010.pdf';
$_blank = true;
$image = '/uploads/2018/12/popc.png';
?>

<?php if($active && ( (isset($testdate) && $testdate) || !isset($testdate) ) ){ ?>
	<script>
		function lacrar(lacrador){
			var popLacrador = document.getElementById(lacrador); 
			popLacrador.setAttribute("style", "display: none;");
		}
	</script>
	<div id="lacrador" class="popup-main"> 
		<div class="popup-overlay"></div> 

		<div class="popup-out" align="center">
		
			<div class="popup-inner">
			
				<div class="popup-body <?= !empty($embed) ? 'video' : 'image' ?>">
					<a href="javascript: lacrar('lacrador');" class="popup-close"> 
						<img src="<?php echo get_template_directory_uri();?>/images/fechar.png">
					</a>
					<a <?= ( $_blank ? 'target="_blank"' : '' ) ?> href="<?= $link ?> ">
						<!--<img src="<?php echo get_template_directory_uri().$image ;?>"> -->
						<img src="https://apform.com.br/wp-content/uploads/2018/12/popc.png">
					</a>
				</div>
			</div> 
		</div>
	</div>
<?php } ?>


<div class="conteudo">
<div id="slider" class="nivoSlider">
    <a href="#"><img src="https://apform.com.br/wp-content/uploads/2019/02/Banner-15-Anos.png"></a>
    <a href="/#contato"><img src="<?php bloginfo( 'template_url' ); ?>/images/banner_formobile.png"></a>
    <a href="https://apform.com.br/landing/"><img src="https://apform.com.br/wp-content/uploads/2019/02/newLanding.png"></a>
    <a href="/#contato"><img src="https://apform.com.br/wp-content/uploads/2017/08/banner_forum.jpg"></a>
    <a href="/#contato"><img src="https://apform.com.br/wp-content/uploads/2017/10/banner_undime.jpg"></a>
    <a href="/escolar/#linha-domus"><img src="https://apform.com.br/wp-content/uploads/2019/02/Banner-Domus.png"></a>
    <a href="/escolar/#cadeiras"><img src="https://apform.com.br/wp-content/uploads/2019/02/Banner-Linha-FNDE.png"></a>
</div>

	    <div class="container">
	    	<div class="row">
				<a class="scroll" href="#produtos"><div class="col-lg-4">
						<div class="boxNav pequenas-areas icons">
							<svg  version="1.1" id="Layer_1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px"
							 height="80px" viewBox="0 0 612 792" enable-background="new 0 0 612 792" xml:space="preserve">
							<g>
								<g>
									<path d="M99.522,689.529c0-205.253,0-410.497,0-615.75c147.433,0,294.866,0,442.3,0c0,205.253,0,410.497,0,615.75
										c-32.967,16.973-53.632,39.848-106.298,23.342c-72.095-22.588-161.691-21.555-234.273,0.567
										C149.928,729.081,130.529,708.391,99.522,689.529z M344.844,126.662c0,179.379,0,353.288,0,526.493c50.676,0,97.4,0,143.037,0
										c0-177.821,0-351.721,0-526.493C438.391,126.662,392.716,126.662,344.844,126.662z M296.729,654.322
										c0-174.645,0-349.628,0-527.305c-48.063,0-94.792,0-143.948,0c0,176.694,0,350.636,0,527.305
										C201.205,654.322,246.901,654.322,296.729,654.322z"/>
									<path fill-rule="evenodd" clip-rule="evenodd" d="M410.658,392.825c0,18.675,1.432,37.52-0.694,55.948
										c-0.868,7.529-9.439,14.169-14.504,21.216c-7.352-5.581-20.821-10.908-21.062-16.778c-1.651-39.797-1.385-79.755,0.749-119.526
										c0.297-5.505,14.275-10.282,21.923-15.397c4.595,7.605,12.195,14.846,13.161,22.892c2.046,16.998,0.623,34.402,0.623,51.646
										C410.79,392.825,410.722,392.825,410.658,392.825z"/>
									<path fill-rule="evenodd" clip-rule="evenodd" d="M231.072,391.547c0-17.294-1.521-34.784,0.686-51.79
										c0.999-7.698,9.401-14.432,14.44-21.605c8.042,6.437,22.596,12.458,23.032,19.369c2.359,37.316,2.147,74.894,0.453,112.277
										c-0.313,6.937-11.442,13.382-17.583,20.047c-6.961-7.368-18.543-13.966-19.932-22.266c-3.053-18.217-0.949-37.299-0.949-56.032
										C231.173,391.547,231.123,391.547,231.072,391.547z"/>
								</g>
							</g>
							</svg>
							<h2>NOSSOS <span class="bold">PRODUTOS</span></h2>
							<p>Confira nossas linhas de produtos.</p>
						</div>
					</div>
				</a>

				<a class="scroll" href="#atuacao">
					<div class="col-lg-4">
						<div class="boxNav pequenas-areas icons">
							<svg version="1.1" id="Capa_1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px"
								 height="90px" viewBox="1.5 1.5 734.569 950.619" enable-background="new 1.5 1.5 734.569 950.619"
								 xml:space="preserve">
								<path fill="#ff0000" d="M389.586,566.582"/>
							<g>
								<path d="M517.267,145.657L517.267,145.657c-83.537-83.538-218.99-83.538-302.54,0l0,0
									c-75.276,75.276-83.75,217.013-19.866,302.187l171.136,247.154l171.137-247.154C601.017,362.67,592.543,220.934,517.267,145.657z
									 M368.08,365.459c-39.003,0-70.615-31.612-70.615-70.615s31.612-70.615,70.615-70.615s70.615,31.612,70.615,70.615
									S407.083,365.459,368.08,365.459z"/>
								<path d="M465.517,646.177l-8.775,19.972c104.758,9.303,180.332,35.749,180.332,66.926
									c0,38.934-117.855,70.496-263.237,70.496c-145.381,0-263.236-31.562-263.236-70.496c0-36.354,102.756-66.277,234.727-70.084
									l-14.398-20.569c-144.78,6.008-256.327,43.63-256.327,89.153c0,49.703,132.965,89.995,296.985,89.995
									c164.021,0,296.985-40.292,296.985-89.995C668.572,691.818,583.5,658.088,465.517,646.177z"/>
							</g>
							</svg>
							<h2>ÁREA DE <span class="bold">ATUAÇÃO</span></h2>
							<p>Conheças os estados que atuamos.</p>
						</div>
				</div>
				</a>

				<a class="scroll" href="#aempresa">
					<div class="col-lg-4">
					<div class="boxNav pequenas-areas icons">
						<svg version="1.1" id="Layer_1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px"
							height="90px" viewBox="0 0 612 792" enable-background="new 0 0 612 792" xml:space="preserve">
						<g>
							<g>
								<path fill-rule="evenodd" clip-rule="evenodd" d="M131.377,396.499c0,27.422,0.799,54.873-0.255,82.251
									c-0.833,21.611,5.914,32.892,29.605,32.766c102.16-0.548,204.324-0.555,306.484-0.015c23.833,0.126,30.205-11.28,29.287-32.884
									c-1.169-27.562-0.284-55.214-0.284-82.821c3.448-1.865,21.337-0.931,24.786-2.796c6.494,9.925,3.945,16.94,4.175,27.006
									c1.58,69.328,1.313,138.73-0.019,208.073c-0.188,9.718-9.459,19.266-14.525,28.895c-131.369,0-262.735,0-394.104,0
									c-4.963-12.234-13.933-24.373-14.218-36.711C100.843,555.968,101.472,460.288,103,396"/>
								<path fill-rule="evenodd" clip-rule="evenodd" d="M487.291,314.374c-45.714,52.127-72.096,52.312-111.03,4.278
									c-19.896,11.568-39.017,31.078-58.926,31.914c-20.08,0.844-40.937-16.838-64.432-27.792
									c-52.705,42.942-69.221,42.439-111.368-9.955c-8.071,9.548-14.625,20.317-23.899,27.659
									c-24.276,19.229-49.255,17.674-71.467-3.664c-22.126-21.249-9.274-37.665,5.769-58.211c34.572-47.242,77.392-62.637,135.844-59.81
									c101.868,4.929,204.136,1.421,306.236,1.665c23.07,0.052,102.261,63.422,101.672,85.174
									c-0.348,12.834-10.624,32.936-20.879,36.326c-18.185,6.01-40.378,4.722-59.644,0.629
									C505.221,340.471,497.96,325.743,487.291,314.374z"/>
								<path fill-rule="evenodd" clip-rule="evenodd" d="M309.789,188.396c-59.159,0.007-118.332,0.666-177.461-0.651
									c-9.726-0.214-19.258-9.111-28.884-13.996c9.703-5.344,19.366-15.239,29.113-15.321c120.826-1.043,241.664-1.029,362.486,0.059
									c9.688,0.089,19.288,10.059,28.925,15.432c-9.719,4.826-19.352,13.626-29.169,13.833
									C433.154,189.033,371.465,188.396,309.789,188.396z"/>
							</g>
						</g>
						</svg>
						<h2>CONHEÇA <span class="bold">A EMPRESA</span></h2>
						<p>Há mais de 12 anos sendo referência no mercado.</p>
					</div>
				</div>
				</a>
			</div>
		</div>

<div id="produtos" class="separador"></div>


	<div class="container">
		<div class="row">
			<h1 class="center" style="margin-bottom:20px;">NOSSOS <span class="bold">PRODUTOS</span></h1>

			<?php query_posts('page_id=9'); if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="center"><?php the_content(); ?></div>
			<?php endwhile; endif; wp_reset_query(); ?>
            
            
            <!-- <div class="col-md-3">
				<a href="<?php echo get_option('home'); ?>/assentos" class="item-produto" style="background-image:url('<?php bloginfo( 'template_url' ); ?>/images/assentos.jpg'); border: 10px solid #616161;">
                	<span class="tl">ASSENTOS</span>
                    <span class="bg"></span>
                </a>
			</div> -->
            
            <!-- <div class="col-md-3">
				<a href="<?php echo get_option('home'); ?>/mesas-e-armarios" class="item-produto" style="background-image:url('<?php bloginfo( 'template_url' ); ?>/images/mesas.jpg'); border: 10px solid #616161;">
                	<span class="tl">MESAS E ARMÁRIOS</span>
                    <span class="bg"></span>
                </a>
			</div> -->
            <div class="col-md-4">
				<a href="<?php echo get_option('home'); ?>/escritorio/" class="link-icon-produtos">
					<div class="circle-item sin-dep wow rollIn">
						<img src="<?php bloginfo( 'template_url' ); ?>/images/icon-escritorio.png" class="img-responsive rotate" style="padding: 45px;">
					</div>
                	<span class="tl text-center title-produto-icon wow slideInLeft">ESCRITÓRIO</span>
                </a>
			</div>

            <div class="col-md-4">
            	<a href="<?php echo get_option('home'); ?>/escolar/" class="link-icon-produtos">
					<div class="circle-item sin-dep wow rollIn">
						<img src="<?php bloginfo( 'template_url' ); ?>/images/icon-escolar.png" class="img-responsive rotate" style="padding: 45px;">
					</div>
                	<span class="tl text-center title-produto-icon wow slideInLeft">ESCOLAR</span>
                </a>
			</div>
			<div class="col-md-4">
				<a href="<?php echo get_option('home'); ?>/linha-urbana/" class="link-icon-produtos">
					<div class="circle-item sin-dep wow rollIn">
						<img src="<?php bloginfo( 'template_url' ); ?>/images/icon-urbano.png" class="img-responsive rotate" style="padding: 45px;">
					</div>
                	<span class="tl text-center title-produto-icon wow slideInLeft">URBANO</span>
                </a>
			</div>
            
            
		</div>
	</div>


	<div id="atuacao" class="separadorTop"></div>
	<div class="atuacao">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-md-offset-1 map">
				<!-- <img src="<?php //bloginfo( 'template_url' ); ?>/images/banner-atuacao.png" class="img-responsive"> -->
				</div>

				<div class="col-md-4 boxAtuacao">
					<h1>ÁREA DE <span class="bold">ATUAÇÃO</h1>
					<?php query_posts('page_id=74'); if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
					<?php endwhile; endif; wp_reset_query(); ?>
				</div>

			</div>

		</div>

			<!-- <div id="aempresa" class="sombraAtuacao"></div> -->
	</div>
			<div id="aempresa" class="separadorTop" style="
    background-image: none;"></div>
		<div class="container">
			<div class="row">
				<h1 class="center">CONHEÇA A <span class="bold">EMPRESA</span></h1>
			</div>



<div class="container">
	<div class="row">
		<?php query_posts('page_id=41'); if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; endif; wp_reset_query(); ?>



		<div id="carousel" class=" carouselEmpresa carousel slide">
			<div class="carousel-inner">
				<div class="item active">
					<div class="row">
          				<?php
						$args = array( 'posts_per_page' => 100, 'category' => 3 );

						$myposts = get_posts( $args );
						$i = 0;
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
          				<?php

						if( $i > 0 && $i%3 == 0 ){
							echo '<div class="item"><div class="row">';
						}
						$imagem = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

						$post_id = $post->ID;
						$key = "link-parceiro";
						$single = true;
						?>
			          	<div class="col-xs-4">
			          		<a href="<?php echo $imagem; ?>" rel="prettyphoto" ><img src="<?php echo $imagem; ?>"></a>
			          	</div>
			          	<?php 
									$i++; 
									if( $i%3 == 0 ){
										echo '</div></div>';
									}
									endforeach; 
									if( $i%3 != 0 ){
										echo '</div></div>';
									}
									wp_reset_postdata();
									?>
						

					</div>
				</div>
			</div>
		</div>
		<div class="navLeft hidden-xs">
			<a data-slide="prev" href="#carousel">
				<img src="<?php bloginfo( 'template_url' ); ?>/images/nav-left.png">
			</a>
		</div>

		<div class="navRight hidden-xs">
			<a data-slide="next" href="#carousel">
				<img src="<?php bloginfo( 'template_url' ); ?>/images/nav-right.png">
			</a>
		</div>


<!--
<div class="center">
		<div id="carousel" class=" carouselEmpresa carousel slide">
			<div class="carousel-inner">
				<div class="item active">


					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
				</div>
				<div class="item">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
				</div>
				<div class="item">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/foto1.jpg">
				</div>
			</div>
		</div>
</div>
-->


		<?php query_posts('page_id=54'); if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; endif; wp_reset_query(); ?>
	</div>
</div>



</div>


<?php // get_sidebar(); ?>
<?php get_footer(); ?>
