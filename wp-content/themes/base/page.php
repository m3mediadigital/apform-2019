<?php get_header(); ?>

	<div class="container margin-top">
		<div class="row">
			<div class="col-md-12">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
					<div class="post" id="post-<?php the_ID(); ?>">

						<h1 class="hPage"><?php the_title(); ?></h1>


							<?php the_content(); ?>

					</div>
					
					<?php endwhile; endif; ?>


			</div>


		</div>

	</div>

<?php get_footer(); ?>
