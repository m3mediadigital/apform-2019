<?php 
/* 	
If you see this text in your browser, PHP is not configured correctly on this hosting provider. 
Contact your hosting provider regarding PHP configuration for your site.

PHP file generated by Adobe Muse CC 2015.2.1.352
*/

require_once('form_process.php');

$form = array(
	'subject' => 'Home Form envio',
	'heading' => 'Envio de novo formulário',
	'success_redirect' => '',
	'resources' => array(
		'checkbox_checked' => 'Marcado',
		'checkbox_unchecked' => 'Desmarcado',
		'submitted_from' => 'Formulário enviado do site: %s',
		'submitted_by' => 'Endereço IP do visitante: %s',
		'too_many_submissions' => 'Muitos envios recentes deste IP',
		'failed_to_send_email' => 'Falha no envio do email',
		'invalid_reCAPTCHA_private_key' => 'Chave privada do reCAPTCHA inválida.',
		'invalid_field_type' => 'Tipo de campo desconhecido \"%s\".',
		'invalid_form_config' => 'O campo \"%s\" possui uma configuração inválida.',
		'unknown_method' => 'Método de solicitação de servidor desconhecido'
	),
	'email' => array(
		'from' => 'jhonnymichel@outlook.com',
		'to' => 'jhonnymichel@outlook.com'
	),
	'fields' => array(
		'custom_U61928' => array(
			'order' => 1,
			'type' => 'string',
			'label' => 'Nome',
			'required' => true,
			'errors' => array(
				'required' => 'O campo \"Nome\" é obrigatório.'
			)
		),
		'Email' => array(
			'order' => 2,
			'type' => 'email',
			'label' => 'Email',
			'required' => true,
			'errors' => array(
				'required' => 'O campo \"Email\" é obrigatório.',
				'format' => 'O campo \"Email\" possui um email inválido.'
			)
		),
		'custom_U61909' => array(
			'order' => 3,
			'type' => 'string',
			'label' => 'CNPJ/CPF',
			'required' => true,
			'errors' => array(
				'required' => 'O campo \"CNPJ/CPF\" é obrigatório.'
			)
		),
		'custom_U61936' => array(
			'order' => 4,
			'type' => 'string',
			'label' => 'Telefone celular',
			'required' => true,
			'errors' => array(
				'required' => 'O campo \"Telefone celular\" é obrigatório.'
			)
		),
		'custom_U61932' => array(
			'order' => 5,
			'type' => 'string',
			'label' => 'Endereço',
			'required' => true,
			'errors' => array(
				'required' => 'O campo \"Endereço\" é obrigatório.'
			)
		),
		'custom_U61900' => array(
			'order' => 6,
			'type' => 'string',
			'label' => 'Cidade',
			'required' => true,
			'errors' => array(
				'required' => 'O campo \"Cidade\" é obrigatório.'
			)
		),
		'custom_U61904' => array(
			'order' => 7,
			'type' => 'string',
			'label' => 'Estado',
			'required' => true,
			'errors' => array(
				'required' => 'O campo \"Estado\" é obrigatório.'
			)
		),
		'custom_U61896' => array(
			'order' => 10,
			'type' => 'string',
			'label' => 'Quantidade',
			'required' => true,
			'errors' => array(
				'required' => 'O campo \"Quantidade\" é obrigatório.'
			)
		),
		'custom_U95836' => array(
			'order' => 9,
			'type' => 'checkboxgroup',
			'label' => 'Material',
			'required' => true,
			'optionItems' => array(
				'Aço',
				'Inox'
			),
			'errors' => array(
				'required' => 'O campo \"Material\" é obrigatório.',
				'format' => 'O campo \"Material\" possui um valor inválido.'
			)
		),
		'custom_U97736' => array(
			'order' => 11,
			'type' => 'checkboxgroup',
			'label' => 'Cor',
			'required' => true,
			'optionItems' => array(
				'Vermelho',
				'Azul',
				'Verde',
				'Cinza'
			),
			'errors' => array(
				'required' => 'O campo \"Cor\" é obrigatório.',
				'format' => 'O campo \"Cor\" possui um valor inválido.'
			)
		),
		'custom_U61913' => array(
			'order' => 8,
			'type' => 'radiogroup',
			'label' => 'Forma de pagamento',
			'required' => true,
			'optionItems' => array(
				'Visa',
				'Master Card',
				'Depósito bancário',
				'Boleto bancário'
			),
			'errors' => array(
				'required' => 'O campo \"Forma de pagamento\" é obrigatório.',
				'format' => 'O campo \"Forma de pagamento\" possui um valor inválido.'
			)
		)
	)
);

process_form($form);
?>
